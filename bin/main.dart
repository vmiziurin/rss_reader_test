import 'dart:convert';
import 'dart:io';

import 'package:firedart/firedart.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart';

import 'rss_reader.dart';

const apiKey = 'AIzaSyCYFottz9j5OuYKqfMfuP0grkrJzdO6IeA';
const projectId = 'temp-rss';
const email = 'vmiziurin@gmail.com';
const password = 'rssreaderTest';

Future main() async {
  await _initFirestore();
  await _initServer();
}

_initServer() async {
  Future<Response> handler(Request request) async {
    if (request.requestedUri.path != '/favicon.ico') {
      var rssNews = await RSSReader().getNewsFromRSS();
      await _storeRSSNewsToBD(rssNews);
      return Response.ok(utf8.encode(_buildNewsLines(rssNews)));
    }
    return Response.notFound("");
  }

  await serve(
      Pipeline().addMiddleware(logRequests()).addHandler(handler),
      InternetAddress.anyIPv4,
      int.tryParse(Platform.environment['PORT'] ?? '8080'));
}

_initFirestore() async {
  FirebaseAuth.initialize(apiKey, VolatileStore());
  Firestore.initialize(projectId);
  var auth = FirebaseAuth.instance;
  await auth.signIn(email, password);
  return Firestore.instance;
}

_storeRSSNewsToBD(rssNews) async {
  for (String date in rssNews.keys) {
    var dbNewsDocRef = Firestore.instance.collection('news').document(date);
    Document dbNewsDoc;
    try {
      dbNewsDoc = await dbNewsDocRef.get();
    } catch (e) {}
    if (dbNewsDoc == null || dbNewsDoc.map[date] != rssNews[date]) {
      await dbNewsDocRef.update({'title': rssNews[date]});
    }
  }
}

_buildNewsLines(Map news) {
  var result = StringBuffer();
  news.forEach((d, t) {
    result.writeln("[$d] $t");
  });
  return result.toString();
}
