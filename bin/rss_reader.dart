import 'dart:collection';

import 'package:http/http.dart' as http;
import 'package:webfeed/webfeed.dart';

class RSSReader {
  static const String FEED_URL =
      'https://rssreadertest.blogspot.com/feeds/posts/default';

  getNewsFromRSS() async {
    var feed = await loadFeed();
    if (null == feed || feed.toString().isEmpty) {
      return {"Empty feed": "No posts yet!"};
    }
    Map<String, String> news = LinkedHashMap();
    feed.items.forEach((item) {
      news[item.published] = item.title;
    });
    return news;
  }

  Future<AtomFeed> loadFeed() async {
    try {
      final xml = await http.read(FEED_URL);
      return AtomFeed.parse(xml);
    } catch (e) {
      print("Exeprion while loading RSS Feed! $e");
    }
    return null;
  }
}
